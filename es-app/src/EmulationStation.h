#pragma once

// These numbers and strings need to be manually updated for a new version.
// Do this version number update as the very last commit for the new release version.
#define PROGRAM_VERSION_STRING "4.1.0-dev"

#define PROGRAM_BUILT_STRING __DATE__ " - " __TIME__
